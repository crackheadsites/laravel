<?php

// First Route method – Root URL will match this method
Route::get('/', function () {
    return view('welcome');
});

// Second Route method – Root URL with ID will match this method
Route::get('ID/{id}',function($id){
    echo 'ID: '.$id;
});

// Third Route method – Root URL with or without name will match this method
Route::get('/user/{name?}',function($name = 'Virat Gandhi'){
    echo "Name: ".$name;
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/create/ticket','TicketController@create');

Route::post('/create/ticket','TicketController@store');

Route::get('/tickets', 'TicketController@index');

Route::get('/edit/ticket/{id}','TicketController@edit');

Route::patch('/edit/ticket/{id}','TicketController@update');

Route::delete('/delete/ticket/{id}','TicketController@destroy');

Route::get('/show/ticket/{id}', 'TicketController@show')->name('ticket');

Route::resource('admin/posts', 'PostsController');