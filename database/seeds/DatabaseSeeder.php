<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i <= 20; $i++) {
        DB::table('posts')->insert([

            'title' => str_random(10),

            'body' => str_random(50)

        ]);

        }

    }
}
