@extends('adminlte::page')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div><br />
        @endif
        <div class="row">
                <div class="col-12 show-title-block">
                    <div><b>Ticket Title:</b></div>
                    <span>{{ $ticket->title }}</span>
                </div>
                <div class="col-12">
                    <div><b>Ticket Description:</b></div>
                    <span>{{ $ticket->description }}</span>
                </div>
        </div>
    </div>
@endsection
